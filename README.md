# GeoIP API Server

A simple GeoIP API Server with MySQL as cache backend. Written in Python using Flask.

## Deploy it

- Environment: Python >=3.8, MySQL 8.x
- Following progress works on Ubuntu 22.04 LTS
- ```
  sudo apt install python3-flask python3-dnspython python3-requests python3-mysql.connector python3-brotli \
  mysql-server nginx uwsgi uwsgi-plugin-python3
  ```
- Copy `config-sample.py` to `config.py` and edit it according to the description
- [Setup uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html#deploying-flask "uWSGI Document") and [NGINX](https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html "uWSGI NGINX Document")
