class db_config:
    # MySQL database configuration
    MYSQL_HOST = 'localhost'
    MYSQL_PORT = 3306
    MYSQL_USER = 'root'
    MYSQL_PASS = '114514'
    MYSQL_DB = 'geoip'

class web_config:
    # Web server configuration
    HOST = 'localhost'
    PORT = 5000

    # If the server is behind a reverse proxy, set this to True to get the real IP address from the X-Forwarded-For header
    # If not set, the default value is False
    REVERSE_PROXY = True
    
class ipregistry_config:
    # Set to True to update hostname, requires python3-dnspython and additional time
    # If set to False, all hostname will be set to None
    UPDATE_HOSTNAME = True

    # Prefix of the table name
    MYSQL_PREFIX = 'ipregistry'

    # Set to True to update recursively if the IP addresses in a subnet have different location information
    # If not set, only the requested IP address will be updated
    UPDATE_RECURSIVE = True

    # Key of the API
    # If not set, the demo key will be used
    # Note that the demo key has a rate limit
    API_KEY = ''