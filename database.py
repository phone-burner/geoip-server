from config import db_config
import mysql.connector as mysql

cnxpool = mysql.pooling.MySQLConnectionPool(
    host=db_config.MYSQL_HOST,
    port=db_config.MYSQL_PORT,
    user=db_config.MYSQL_USER,
    passwd=db_config.MYSQL_PASS,
    database=db_config.MYSQL_DB,
    pool_name='cnxpool',
    pool_size=32,
    pool_reset_session=True
)