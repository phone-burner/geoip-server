#!/bin/env python3
import ipaddress
from ipaddress import IPv4Address, IPv6Address, IPv4Network, IPv6Network
from config import ipregistry_config
from database import cnxpool
import requests
import random
import datetime
import json
import typing
import threading
import time


# Format the IP address
# If the IP address is IPv6, convert it to the first IP in the /64 network


def format_ip(ip: str | IPv4Address | IPv6Address) -> IPv4Address | IPv6Address:
    ip_n = ipaddress.ip_address(ip)
    if ip_n.version == 4:
        return ip_n
    else:
        return ipaddress.ip_network(str(ip) + '/64', strict=False)[0]

# Update the database
# Strategy: In an /24 network, we only need to check 3 IP addresses: the first(.1), the last(.254),
# and the requested one (randomly selected if the requested one is .1 or .254)
# If their info are the same, we can assume the whole network is the same type.
# If not, we need to check the whole network or just update the requested one.


updating_ips = []


class update_ip_block_background(threading.Thread):
    def __init__(self, network: IPv4Network, results: typing.List[dict], interactive: bool = True):
        threading.Thread.__init__(self)
        self.network = network
        self.interactive = interactive
        self.results = results

    def run(self):
        global updating_ips
        if self.results[0]['location']['city'] == self.results[1]['location']['city'] == self.results[2]['location']['city']:
            # Insert the network into the database
            for i in self.network:
                self.results[0]['ip'] = str(i)
                update_ip_db(i, self.results[0].copy())
                updating_ips.remove(i)
        else:
            if ipregistry_config.UPDATE_RECURSIVE:
                # Check the whole network
                for i in self.network:
                    update_ip_db(i, request_ip(i, self.interactive))


def update_ip(ip: IPv4Address | IPv6Address, interactive: bool = True):
    ip = format_ip(ip)
    if ip.version == 4:
        network = ipaddress.ip_network(str(ip) + '/24', strict=False)
        ips = [IPv4Address] * 3
        ips[0] = network[0]
        ips[1] = network[-2]  # -1 is the broadcast address
        ips[2] = network[random.randrange(
            1, 253)] if ip == network[0] or ip == network[-2] else str(ip)
        print('Checking IP range: ' + str(network))
        results = [None] * 3
        for i in ips:
            results[ips.index(i)] = request_ip(i, interactive)
        if {} in results:
            return

        # Insert the requested IP first and then the other IPs in the background
        update_ip_db(ip, results[ips.index(ip)])
        global updating_ips
        for i in network:
            updating_ips.append(i)
        update_ip_block_background(network, results, interactive).start()

    else:
        # Query 1 time only and apply to the /64 network
        update_ip_db(ip, request_ip(ip, interactive))


def request_ip(ip: IPv4Address | IPv6Address, interactive: bool = True) -> dict:
    if ipregistry_config.API_KEY is None:
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
            "Origin": "https://ipregistry.co",
            "Referer": "https://ipregistry.co/",
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9",
            "Connection": "keep-alive",
        }
        res = requests.get('https://api.ipregistry.co/' + str(ip) +
                           '?hostname=true&key=sb69ksjcajfs4c', headers=headers)
    else:
        res = requests.get('https://api.ipregistry.co/' + str(ip) +
                           '?hostname=true&key=' + ipregistry_config.API_KEY)
    try:
        if res.status_code == 200:
            return res.json()
        else:
            raise Exception('Status code: ' + str(res.status_code))
    except Exception as e:
        print('Error: ')
        print('Response: ' + res.text)
        print('Exception: ' + str(e))
        if interactive:
            print('Try again? (y/n)')
            if input() == 'y':
                return request_ip(ip)
        return {}

# Database structure:
#     Table `ip`:
#         `ip` varchar(15) NOT NULL to store the IP address as key
#         `security` json NOT NULL to store the security information
#         `country` varchar(2) NOT NULL to store the country code
#         `continent` varchar(2) NOT NULL to store the continent code
#         `timezone` varchar(32) NOT NULL to store the timezone
#         `currency` varchar(3) NOT NULL to store the 3-char currency code
#         `parent` varchar(20) NOT NULL to store the parent IP block
#         `location` json NOT NULL to store the location information except for the country
#     Table `country`:
#         `country` varchar(2) NOT NULL to store the country code as key
#         `country_info` json NOT NULL to store the country information
#     Table `currency`:
#         `currency` varchar(3) NOT NULL to store the 3-char currency code as key
#         `currency_info` json NOT NULL to store the currency information
#     Table `timezone`:
#         `timezone` varchar(32) NOT NULL to store the timezone as key
#         `timezone_info` json NOT NULL to store the timezone information
#     Table `continent`:
#         `continent` varchar(2) NOT NULL to store the continent code as key
#         `continent_info` json NOT NULL to store the continent information
#     Table `asn`:
#         `asn` int(10) unsigned NOT NULL to store the ASN as key
#         `asn_info` json NOT NULL to store the ASN information
#         `company` json NOT NULL to store the ASN company information
#     Table `ip_parent`:
#         `parent` varchar(20) NOT NULL to store the IP address block as key
#         `asn` int(10) unsigned NOT NULL to store the ASN
#         `carrier` json NOT NULL to store the carrier information


def init_database():
    cnx = cnxpool.get_connection()
    cursor = cnx.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_ip` (' % ipregistry_config.MYSQL_PREFIX +
                   '`ip` varchar(32) NOT NULL,' +
                   '`continent` varchar(2) NOT NULL,' +  # Continent code
                   '`country` varchar(2) NOT NULL,' +  # Country code
                   '`currency` varchar(3) NOT NULL,' +  # Currency code
                   '`location` json NOT NULL,' +
                   '`security` json NOT NULL,' +
                   '`timezone` varchar(32) NOT NULL,' +
                   '`parent` varchar(32) NOT NULL,' +
                   'PRIMARY KEY (`ip`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_country` (' % ipregistry_config.MYSQL_PREFIX +
                   '`country` varchar(2) NOT NULL,' +
                   '`country_info` json NOT NULL,' +
                   'PRIMARY KEY (`country`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_currency` (' % ipregistry_config.MYSQL_PREFIX +
                   '`currency` varchar(3) NOT NULL,' +
                   '`currency_info` json NOT NULL,' +
                   'PRIMARY KEY (`currency`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_timezone` (' % ipregistry_config.MYSQL_PREFIX +
                   '`timezone` varchar(32) NOT NULL,' +
                   '`timezone_info` json NOT NULL,' +
                   'PRIMARY KEY (`timezone`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_continent` (' % ipregistry_config.MYSQL_PREFIX +
                   '`continent` varchar(2) NOT NULL,' +
                   '`continent_info` json NOT NULL,' +
                   'PRIMARY KEY (`continent`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_asn` (' % ipregistry_config.MYSQL_PREFIX +
                   '`asn` int(10) unsigned NOT NULL,' +
                   '`asn_info` json NOT NULL,' +
                   '`company` json NOT NULL,' +
                   'PRIMARY KEY (`asn`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cursor.execute('CREATE TABLE IF NOT EXISTS `%s_parent` (' % ipregistry_config.MYSQL_PREFIX +
                   '`parent` varchar(32) NOT NULL,' +
                   '`asn` int(10) unsigned NOT NULL,' +
                   '`carrier` json NOT NULL,' +
                   'PRIMARY KEY (`parent`)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;')
    cnx.commit()


# r = True to prevent recursion
def get_ip_info(ip: IPv4Address | IPv6Address, r=False) -> typing.Optional[dict]:
    # Format IPv6 to /64 subnet as key in database
    ip_f = str(format_ip(ip)) + '/64' if ip.version == 6 else str(format_ip(ip))

    # Query database
    cnx = cnxpool.get_connection()
    cursor = cnx.cursor()
    cursor.execute('SELECT * FROM `%s_ip` WHERE `ip` = "%s"' %
                   (ipregistry_config.MYSQL_PREFIX, ip_f))
    ip_res = cursor.fetchone()

    if ip_res is None:  # IP not in database
        global updating_ips
        if ip in updating_ips:  # In updating
            # Wait for 1 second
            time.sleep(1)
            return get_ip_info(ip, False)
        if r == True:  # Prevent recursion
            return None
        update_ip(ip, False)
        return get_ip_info(ip, True)
    else:
        # Check hostname
        if ipregistry_config.UPDATE_HOSTNAME == True:
            import dns.resolver
            import dns.reversename
            try:
                hostname = str(dns.resolver.query(
                    dns.reversename.from_address(str(ip)), 'PTR')[0]).rstrip('.')
            except dns.resolver.NXDOMAIN:
                hostname = ''
            except dns.resolver.NoAnswer:
                hostname = ''
            except Exception as e:
                print('Error while resolving hostname:' + str(e))
                hostname = ''
        else:
            hostname = ''
        # Query other tables
        cursor.execute('SELECT * FROM `%s_continent` WHERE `continent` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, ip_res[1]))
        continent_res = cursor.fetchone()
        cursor.execute('SELECT * FROM `%s_country` WHERE `country` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, ip_res[2]))
        country_res = cursor.fetchone()
        cursor.execute('SELECT * FROM `%s_currency` WHERE `currency` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, ip_res[3]))
        currency_res = cursor.fetchone()
        cursor.execute('SELECT * FROM `%s_timezone` WHERE `timezone` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, ip_res[6]))
        timezone_res = cursor.fetchone()
        timezone_info_obj = json.loads(timezone_res[1])
        cursor.execute('SELECT * FROM `%s_parent` WHERE `parent` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, ip_res[7]))
        parent_res = cursor.fetchone()
        cursor.execute('SELECT * FROM `%s_asn` WHERE `asn` = "%s"' %
                       (ipregistry_config.MYSQL_PREFIX, parent_res[1]))
        asn_res = cursor.fetchone()
        asn_info_obj = json.loads(asn_res[1])

        # Make the final result
        info = {
            'ip': ip,
            'type': "IPv4" if ip.version == 4 else "IPv6",
            'hostname': hostname,
            'carrier': json.loads(parent_res[2]),
            'company': json.loads(asn_res[2]),
            'connection': {
                'asn': parent_res[1],
                'domain': asn_info_obj['domain'],
                'organization': asn_info_obj['organization'],
                'route': ip_res[7],
                'type': asn_info_obj['type']
            },
            'currency': json.loads(currency_res[1]),
            'location': json.loads(ip_res[4]),
            'security': json.loads(ip_res[5]),
            'timezone': json.loads(timezone_res[1]),
        }
        info['location']['continent'] = json.loads(continent_res[1])
        info['location']['country'] = json.loads(country_res[1])
        info['timezone']['current_time'] = (datetime.datetime.utcnow(
        ) + datetime.timedelta(seconds=(int)(timezone_info_obj['offset']))).strftime('%Y-%m-%dT%H:%M:%S%z')

        cursor.close()
        cnx.close()
        return info


def update_ip_db(ip: IPv4Address | IPv6Address, info: dict):
    ip = format_ip(ip)
    if info is None or info == {}:  # No info
        return
    try:
        ip_s = str(ip)
        # Store IPv6 as /64 subnet
        if ip.version == 6:
            ip_s += '/64'

        # Extract the info
        # Table: country
        country_info = json.dumps(info['location']['country'])

        # Table: continent
        continent_info = json.dumps(info['location']['continent'])

        # Table: ip
        continent = info['location']['continent']['code']
        country = info['location']['country']['code']
        currency = info['currency']['code']
        # Remove unnecessary information
        location_l = info['location'].copy()
        location_l.pop('country')
        location_l.pop('continent')
        location = json.dumps(location_l)
        security = json.dumps(info['security'])
        timezone = info['time_zone']['id']
        parent = info['connection']['route']

        # Table: currency
        currency_info = json.dumps(info['currency'])

        # Table: timezone
        timezone_info_l = info['time_zone'].copy()
        timezone_info_l.pop('current_time')
        timezone_info = json.dumps(timezone_info_l)

        # Table: parent
        carrier = json.dumps(info['carrier'])

        # Table: asn
        asn = info['connection']['asn']
        asn_info_l = info['connection'].copy()
        asn_info_l.pop('asn')
        asn_info = json.dumps(asn_info_l)
        company = json.dumps(info['company'])

    except KeyError:
        print('Wrong format: %s' % info)
        return

    # Insert into database
    # Usually country, continent, currency and timezone are not changed, so we can ignore them
    cnx = cnxpool.get_connection()
    cursor = cnx.cursor()
    cursor.execute(f'INSERT INTO `{ipregistry_config.MYSQL_PREFIX}_ip` (`ip`, `continent`, `country`, `currency`, `location`, `security`, `timezone`, `parent`) \
                   VALUES (%s, %s, %s, %s, %s, %s, %s, %s) \
                    ON DUPLICATE KEY UPDATE `continent` = %s, `country` = %s, `currency` = %s, `location` = %s, `security` = %s, `timezone` = %s, `parent` = %s;',
                   (ip_s, continent, country, currency, location, security, timezone, parent,
                    continent, country, currency, location, security, timezone, parent))
    cursor.execute(f'INSERT IGNORE INTO `{ipregistry_config.MYSQL_PREFIX}_country` (`country`, `country_info`) \
                    VALUES (%s, %s);',
                   (country, country_info))
    cursor.execute(f'INSERT IGNORE INTO `{ipregistry_config.MYSQL_PREFIX}_continent` (`continent`, `continent_info`) \
                    VALUES (%s, %s);',
                   (continent, continent_info))
    cursor.execute(f'INSERT IGNORE INTO `{ipregistry_config.MYSQL_PREFIX}_currency` (`currency`, `currency_info`) \
                    VALUES (%s, %s);',
                   (currency, currency_info))
    cursor.execute(f'INSERT IGNORE INTO `{ipregistry_config.MYSQL_PREFIX}_timezone` (`timezone`, `timezone_info`) \
                    VALUES (%s, %s);',
                   (timezone, timezone_info))
    cursor.execute(f'INSERT INTO `{ipregistry_config.MYSQL_PREFIX}_parent` (`parent`, `asn`, `carrier`) \
                    VALUES (%s, %s, %s) \
                    ON DUPLICATE KEY UPDATE `asn` = %s, `carrier` = %s;',
                   (parent, asn, carrier, asn, carrier))
    cursor.execute(f'INSERT INTO `{ipregistry_config.MYSQL_PREFIX}_asn` (`asn`, `asn_info`, `company`) \
                    VALUES (%s, %s, %s) \
                    ON DUPLICATE KEY UPDATE `asn_info` = %s, `company` = %s;',
                   (asn, asn_info, company, asn_info, company))
    cnx.commit()
    cursor.close()
    cnx.close()


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Update IP address database.')
    pa = parser.add_mutually_exclusive_group(required=True)
    pa.add_argument('-i', '--ip-range',
                    help='IP range to update, in CIDR notation.')
    args = parser.parse_args()

    # Check if IP range is valid
    if args.ip_range is not None:
        try:
            ipaddress.ip_network(args.ip_range)
        except ValueError:
            print('Invalid IP range.')
            exit(1)
    else:
        print('Not implemented yet.')
        exit(1)

    # TODO Implement it.


if __name__ == '__main__':
    main()
