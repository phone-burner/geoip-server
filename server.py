from flask import Flask, request, jsonify
from config import web_config
import ipaddress
from ipregistry import get_ip_info
import json


api = Flask(__name__)


@api.route('/api/v1/t1', methods=['GET'])  # ipregistry
def check_ip():

    # Get the IP address from the request
    if web_config.REVERSE_PROXY:
        ip = request.args.get('ip', request.headers.get(
            'X-Forwarded-For', request.remote_addr))
    else:
        ip = request.args.get('ip', request.remote_addr)

    if ip is None:
        return jsonify({'status': 'error', 'message': 'No IP address provided.'})

    # Check if the IP address is valid
    try:
        ip_a = ipaddress.ip_address(ip)

        # Check if the IP address is publicly routable
        if not ip_a.is_global:
            return jsonify({'status': 'error', 'message': 'Private or reserved IP address.'})

    except ValueError:
        return jsonify({'status': 'error', 'message': 'Invalid IP address.'})

    result = get_ip_info(ip_a)
    if result is None:
        return jsonify({'status': 'error', 'message': 'IP address not found.'})
    return jsonify(result)


if __name__ == '__main__':
    from ipregistry import init_database
    init_database()
    api.run(host=web_config.HOST, port=web_config.PORT)
