#!/bin/env python3
import argparse
import ipaddress
import mysql.connector as mysql
from config import db_config
import requests
import random
import json

db = mysql.connect(
    host=db_config.MYSQL_HOST,
    port=db_config.MYSQL_PORT,
    user=db_config.MYSQL_USER,
    passwd=db_config.MYSQL_PASS,
    database=db_config.MYSQL_DB
)

# Update the database
# Strategy: In an /24 network, we only need to check 3 IP addresses: the first(.1), the last(.254), and a random one.
# If their info are the same, we can assume the whole network is the same type.
# If not, we need to check the whole network.
def update_ip(network: ipaddress.IPv4Network, interactive: bool = True):
    if network.prefixlen < 24:
        for subnet in network.subnets(new_prefix=24):
            update_ip(subnet)

    elif network.prefixlen > 30:
        print('Invalid IP range.')
    else:
        ips = [''] * 3
        ips[0] = str(network[0])
        ips[1] = str(network[-2])
        ips[2] = str(network[random.randrange(2, network.num_addresses - 3)])
        print('Checking IP range: ' + str(network))
        results = [None] * 3
        for ip in ips:
            results[ips.index(ip)] = update_single_ip(ip, interactive)
        if {} in results:
            return
        if results[0]['city'] == results[1]['city'] == results[2]['city']:
            cursor = db.cursor()
            for ip in network:
                results[0]['ip'] = str(ip)
                cursor.execute('INSERT INTO `ipv4` (`ip`, `city`, `region`, `country`, `loc`, `org`, `timezone`) VALUES (%(ip)s, %(city)s, %(region)s, %(country)s, %(loc)s, %(org)s, %(timezone)s) ON DUPLICATE KEY UPDATE `city` = %(city)s, `region` = %(region)s, `country` = %(country)s, `loc` = %(loc)s, `org` = %(org)s, `timezone` = %(timezone)s', results[0])
            db.commit()
            print('Updated.')
        else:
            # We need to check the whole network
            for ip in network:
                res = update_single_ip(str(ip), interactive)
                if res == {}:
                    continue
                cursor = db.cursor()
                cursor.execute('INSERT INTO `ipv4` (`ip`, `city`, `region`, `country`, `loc`, `org`, `timezone`) VALUES (%(ip)s, %(city)s, %(region)s, %(country)s, %(loc)s, %(org)s, %(timezone)s) ON DUPLICATE KEY UPDATE `city` = %(city)s, `region` = %(region)s, `country` = %(country)s, `loc` = %(loc)s, `org` = %(org)s, `timezone` = %(timezone)s', res)
                db.commit()
                    
def update_single_ip(ip: str, interactive: bool = True) -> dict:
    res = requests.get('http://ipinfo.io/' + ip)
    if res.status_code == 200:
        return res.json()
    else:
        print('Error: ' + res.text)
        if interactive:
            print('Try again? (y/n)')
            if input() == 'y':
                return update_single_ip(ip)
        return {}



def main():
    parser = argparse.ArgumentParser(description='Update IP address database.')
    pa = parser.add_mutually_exclusive_group(required=True)
    pa.add_argument('-i', '--ip-range', help='IP range to update, in CIDR notation.')
    #pa.add_argument('-c', '--cidr', help='Minumum CIDR to update, defaults to 24.', type=int, default=24)
    #pa.add_argument('-f', '--file', help='File containing IP ranges to update, one per line.')
    args = parser.parse_args()

    # Check if IP range is valid
    if args.ip_range is not None:
        try:
            ipaddress.ip_network(args.ip_range)
        except ValueError:
            print('Invalid IP range.')
            exit(1)
    else:
        print('Not implemented yet.')
        exit(1)

    update_ip(ipaddress.ip_network(args.ip_range))

if __name__ == '__main__':
    main()